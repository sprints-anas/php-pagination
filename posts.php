<?php
require_once('config.php');
require_once(BASE_PATH . '/logic/posts.php');
require_once(BASE_PATH . '/logic/categories.php');

if(isset($_REQUEST['page'])) {
    $current_page = $_REQUEST['page'];
}
else {
    $current_page = 1;
}

$page_size = 6;
$posts = getPosts($page_size, $current_page);

$SQL = "SELECT count(0) as _count FROM posts JOIN users ON users.id=posts.user_id JOIN categories ON categories.id=posts.category_id";
$posts_count  = getRow($SQL)['_count'];
$offset = ceil($posts_count / $page_size);

function getUrl($page)
{
    return BASE_URL . "/posts.php?page=$page";
}
?>
<?php require_once('layout/header.php'); ?>
<!-- Page Content -->
<!-- Banner Starts Here -->
<div class="heading-page header-text">
    <section class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-content">
                        <h4>Recent Posts</h4>
                        <h2>Our Recent Blog Entries</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Banner Ends Here -->
<section class="blog-posts">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="sidebar-item search">
                    <form id="search_form" name="gs" method="GET" action="<?= BASE_URL . '/posts.php' ?>">
                        <input type="text" value="<?= isset($_REQUEST['q']) ? $_REQUEST['q'] : '' ?>" name="q" class="searchText" placeholder="type to search..." autocomplete="on">
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="all-blog-posts">
                    <div class="row">
                        <?php
                        foreach ($posts as $post) {
                        ?>
                            <div class="col-lg-6">
                                <div class="blog-post">
                                    <div class="blog-thumb">
                                        <img src="<?= BASE_URL . $post['image'] ?>" alt="">
                                    </div>
                                    <div class="down-content">
                                        <span><?= $post['category_name'] ?></span>
                                        <a href="<?= BASE_URL . '/post-details.php?id=' . $post['id'] ?>">
                                            <h4><?= $post['title'] ?></h4>
                                        </a>
                                        <ul class="post-info">
                                            <li><a href="#"><?= $post['user_name'] ?></a></li>
                                            <li><a href="#"><?= $post['publish_date'] ?></a></li>
                                            <li><a href="#"><?= $post['comments'] ?> Comments</a></li>
                                        </ul>
                                        <p><?= $post['content'] ?></p>
                                        <?php
                                        if ($post['tags']) {
                                        ?>
                                            <div class="post-options">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <ul class="post-tags">
                                                            <li><i class="fa fa-tags"></i></li>
                                                            <?php
                                                            foreach ($post['tags'] as $tag) {
                                                            ?>
                                                                <li><a href="<?= BASE_URL."/posts.php?tag_id={$tag['id']}"?>"><?= $tag['name'] ?></a></li>
                                                            <?php
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                        ?>

                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>

                    </div>
                                    
                    <div class="col-lg-12">
                        <ul class="page-numbers">
                            <?php
                            $prev_page = getUrl($current_page - 1);
                            if ($current_page > 1) {
                                echo "<li><a class='fa fa-angle-double-left' href='{$prev_page}'></a></li>";
                            }

                            for ($i = 1; $i <= $offset; $i++) {
                                $url = getUrl($i);
                                if($i == $current_page) echo "<li class='active'><a href='{$url}'>{$i}</a></li>";
                                else echo "<li><a href='{$url}'>{$i}</a></li>";                               
                            }
                            
                            $next_page = getUrl($current_page + 1);
                            if ($current_page < $offset) {
                                echo "<li><a class='fa fa-angle-double-right' href='{$next_page}'></a></li>";
                            }
                            ?>
                        </ul>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</section>

<?php require_once('layout/footer.php') ?>